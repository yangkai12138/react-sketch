import * as React from 'react';
import { render, Artboard, Text, View, makeSymbol } from 'react-sketchapp';

const TextComponent = ({title}) => (
    <Text
        name="list/name"
        style={{
            fontSize: 16,
            fontFamily: 'Helvetica',
            fontWeight: 'bold',
            color: '#000000',
        }}
    >
        {title}
    </Text>
);
// const TextSymbol = makeSymbol(TextComponent);
const OuterDiv = () => (
    <View
        name="outerDiv"
        style={{
            width: 360,
            height: 236,
            background: '#ffffff',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'start',
            justifyContent: 'space-around',
            paddingLeft: 20
        }}
    >
        <TextComponent title={'新北区太湖东路创意产业园9-4'}/>
        <TextComponent title={'南大街人民公园'}/>
        <TextComponent title={'怀德桥吾悦国际'}/>
        <TextComponent title={'西新桥'}/>
        <TextComponent title={'常州市钟楼区人民政府欢迎你'}/>
    </View>
);

const OutDivSymbol = makeSymbol(OuterDiv);

const Document = () => (
  <Artboard
    name="Swatches"
    style={{
      flexDirection: 'row',
      flexWrap: 'wrap',
      width: (96 + 8) * 4,
    }}
  >
    <OutDivSymbol/>
  </Artboard>
);


export default () => {
  render(<Document/>, context.document.currentPage());
};
