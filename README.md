# Basic setup

## How to use


Install the dependencies

```bash
npm install
```

Then, open Sketch and navigate to `Plugins → react-sketchapp: Basic skpm Example`

Run with live reloading in Sketch, need a new sketch doc open

```bash
npm run render
```

## The idea behind the example

[`skpm`](https://github.com/skpm/skpm) is the easiest way to build `react-sketchapp` projects - this is a minimal example of it in use.

